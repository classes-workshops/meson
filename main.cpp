/*
 * Copyright 2021
 * Author: Luis G. Leon-Vega <lleon95@gmail.com>
 * See LICENSE for licensing information
 */

/* This helps to stringify any macro coming from compilation flags */
#define xstr(a) str(a)
#define str(a) #a

/* 
 * This handles the compilation flags with -DMESSAGE
 * If you do not define MESSAGE, it will default to "Hello World"
 * If you define it, it will stringify it and define the macro
 * accordingly
 */
#ifndef MESSAGE
#define CUSTOM_MESSAGE "Hello World!"
#else
#define CUSTOM_MESSAGE xstr(MESSAGE)
#endif

#include "writers.hpp"

int main() {
  /* Get an abstract implementation of Screen and File Writers */
  auto filewriter = IWriter::Factory(Writers::File);
  auto screenwriter = IWriter::Factory(Writers::Screen);

  /* Write message */
  filewriter->Write(CUSTOM_MESSAGE, "text.txt");
  screenwriter->Write(CUSTOM_MESSAGE);

  return 0;
}
