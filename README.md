# Meson

## Instructions

Please, find some information in:

* [main.cpp](./main.cpp)
* [meson.build](./meson.build)
* [meson_options.txt](./meson_options.txt)
* [src/meson.build](./src/meson.build)

The idea of this tutorial is to:

* Identify yourself with meson with a simple example
* How to use some wildcards
* How to compile a project with multiple source files and link them altogether.

This project can be used for C/C++ projects without any issue

## Compiling

Building the project with defaults:

```bash
meson builddir
ninja -C builddir
```

For passing a custom message:

```bash
# Define the option
meson builddir -Dmessage='Bonjour Monde!'
ninja -C builddir
```

Executing:

```bash
./builddir/main
```

Cleaning:

```bash
rm -r builddir
```

Author: Luis G Leon-Vega

MIT License 2021
