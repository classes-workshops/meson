/*
 * Copyright 2021
 * Author: Luis G. Leon-Vega <lleon95@gmail.com>
 * See LICENSE for licensing information
 */

/* This is thanks to the -I./include flag */
#include "writers.hpp"

#include "filewriter.hpp"
#include "screenwriter.hpp"

std::shared_ptr<IWriter> IWriter::Factory(const Writers writer) {
  switch(writer) {
    case Writers::Screen:
      return std::make_shared<ScreenWriter>();
    case Writers::File:
      return std::make_shared<FileWriter>();
    default:
      return nullptr;
  }
}
