/*
 * Copyright 2021
 * Author: Luis G. Leon-Vega <lleon95@gmail.com>
 * See LICENSE for licensing information
 */

/* This is thanks to the -I./include flag */
#include "filewriter.hpp"

#include <fstream>
#include <iostream>
#include <stdexcept>

bool FileWriter::open() {
  if (is_open_) return true;

  try {
    this->outfile_.open(this->path_);
  } catch(const std::exception & e) {
    this->is_open_ = false;
    std::cerr << "Cannot open the file: " << e.what() << std::endl;
  }

  this->is_open_ = this->outfile_.is_open();
  return this->is_open_;
}

void FileWriter::Write(const std::string &msg) {
  if (path_.empty()) {
    throw std::runtime_error("Path unspecified");
  }

  if (!this->open()) {
    throw std::runtime_error("Could not open the file");
  }

  this->outfile_ << msg << std::endl;
}

void FileWriter::Write(const std::string &msg, const std::string &path) {
  this->path_ = path;

  if (this->path_.empty()) {
    throw std::runtime_error("Path unspecified");
  }

  if (!this->open()) {
    throw std::runtime_error("Could not open the file");
  }

  this->outfile_ << msg << std::endl;
}

FileWriter::FileWriter(const std::string &path) : path_{path} {
  this->open();
}

FileWriter::~FileWriter() {
  this->outfile_.close();
}
